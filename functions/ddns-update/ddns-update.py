#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import ipaddress
import boto3
import datetime

# weblinks:
# 2018-12-18: https://goonan.io/a-simple-python3-lambda-function-on-aws-with-an-api-gateway/
# 2018-12-18: https://aws.amazon.com/premiumsupport/knowledge-center/malformed-502-api-gateway/


def arecord(zoneid, comment, name, ip):
    """ create or update the an A record """
    client = boto3.client('route53')

    print("Send route 53 request for A recors {} with ip {}".format(name,ip))
    response = client.change_resource_record_sets(
        HostedZoneId = zoneid,
        ChangeBatch = {
            'Comment': comment,
            'Changes': [{
                'Action': 'UPSERT',
                'ResourceRecordSet': {
                    'Name': name,
                    'Type': 'A',
                    'TTL': 300,
                    'ResourceRecords': [
                        { 'Value': ip }
                    ]
                }
            }]
        }
    )

    print("Route53 response: {}".format(response))

def response(message, status_code, base64_encoded=False):
    """ send response to api gateway """
    return {
        'isBase64Encoded': base64_encoded,
        'statusCode': str(status_code),
        'body': json.dumps(message),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }

def handler(event, context):
    """ 
        lambda event handler
    """ 
    
    print("Start ddns update process")

    try:
        # get the required variables in place
        source_ip = event['requestContext']['identity']['sourceIp']
        domain = os.environ['DOMAIN']
        zoneid = os.environ['ZONEID']
        blacklist = os.getenv('BLACKLIST','').split(',')
        body = json.loads(event['body'])

        # if no name is specified we cant create a dns entry
        if not body['name']:
            raise ValueError("invalid name")
        else:
            record_name = body['name']

        # check if the record name is in the blacklist
        if record_name in blacklist:
            print("Record {} is in blacklist {}. Abort.".format(record_name,blacklist))
            return response({'message': 'Invalid name specified. Abort.'}, 400) 

        # if no ip field is set in the message body we take
        # the public ip
        record_ip = source_ip
        if 'ip' in body:
            if body['ip']:
                record_ip = body['ip']
                
        # check if the ip address is valid
        try:
            ipaddress.ip_address(record_ip)
        except Exception as e:
            print("Invalid ip address sepcified - {}. Abort.".format(e))
            return response({'message': 'Invalid ip specified. Abort.'}, 400) 

        # create the comment string for the dns record
        record_comment = "Last update: {} UTC".format((datetime.datetime.utcnow()))

        # name and ip is valid. lets create or update the dns record
        arecord(zoneid, record_comment, "{}.{}.".format(record_name,domain), record_ip)
        
        # send a ok to the client
        return response({'message': 'Sent DNS update request. Finished.'}, 200)
    except Exception as e:
        print("Unhandled exception: {}".format(e))
        return response({'message': 'Invalid request'}, 500)
