#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import base64

# weblinks:
# 2018-12-18: https://goonan.io/a-simple-python3-lambda-function-on-aws-with-an-api-gateway/
# 2018-12-18: https://aws.amazon.com/premiumsupport/knowledge-center/malformed-502-api-gateway/
# 2018-12-18: https://medium.com/@Da_vidgf/http-basic-auth-with-api-gateway-and-serverless-5ae14ad0a270
# 2018-12-22: https://stackoverflow.com/questions/41486130/aws-api-gateway-execution-failed-due-to-configuration-error-invalid-json-in-re
# 2018-12-22: https://github.com/awslabs/aws-apigateway-lambda-authorizer-blueprints/blob/master/blueprints/python/api-gateway-authorizer-python.py

def policy(event, principalId):
    """ create a policy document """
    tmp = event['methodArn'].split(':')
    awsAccountId = tmp[4]
    apiGatewayArnTmp = tmp[5].split('/')
    awsRegion = tmp[3]
    restApiId = apiGatewayArnTmp[0]
    stage = apiGatewayArnTmp[1]
    apiArn = 'arn:aws:execute-api:' + awsRegion + ':' + awsAccountId + ':' + restApiId + '/' + stage + '/*/*'

    policy = {
        'principalId': principalId,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Action': 'execute-api:Invoke',
                    'Effect': 'Allow',
                    'Resource': [
                        apiArn
                    ],
                }
            ]
        }
    }

    return policy

def handler(event, context):
    """ 
        lambda event handler
    """

    try:
        print("Start authorization process")

        # check if authorization header isnt set
        if 'Authorization' not in event['headers']:
            print("No Authorization header found. Abort.")
            raise Exception('Unauthorized')
      
        # get username and password from envrionment
        auth_username = os.environ['AUTH_USERNAME']
        auth_password = os.environ['AUTH_PASSWORD']

        # get password username and password from auth header
        authorizationHeader = event['headers']['Authorization']
        encodedCreds = authorizationHeader.split(' ')[1]
        plainCreds = base64.b64decode(encodedCreds).decode("utf-8").split(':')
        cred_username = plainCreds[0]
        cred_password = plainCreds[1]

        # check if credentials match
        if auth_username == cred_username and auth_password == cred_password:
            print("Authentication succeded. Continue.")
            principalId = "mikrotik|{}|{}".format(cred_username,event['requestContext']['identity']['sourceIp'])
            return policy(event, principalId)

        else:
            print("Wrong username or password. Abort.")
            raise Exception('Unauthorized')

    except Exception as e:
        print("Unhandled exception: {}".format(e))
        raise Exception('Unauthorized')
