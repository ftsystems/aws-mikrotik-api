# mikrotik-api

Self-made mikrotik dyndns - based on AWS lambda and api gateway

The setup creates A records for the given public ips of a mikrotik router.
The records are created in the dns zone "mikrotik.ftscloud.ch".

## mikrotik fetch
To update the ip address from a mikrotik router you need to use the fetch command
```bash
/tool fetch http-method=post url="https://api.mikrotik.ftscloud.ch/ddns/update" user=admin password=secret http-content-type="application/json" http-data="{\"name\":\"publicdnsname\"}"
```

By default the api takes the source ip the request is sent from (aka the public ip of the router for the A record.
You can override the ip address to set with the ip field.
```bash
/tool fetch http-method=post url="https://api.mikrotik.ftscloud.ch/ddns/update" user=admin password=secret http-content-type="application/json" http-data="{\"name\":\"customiprecord\", \"ip\":\"192.168.1.1\"}"
```

The dns entries are available after a few seconds:
```bash
$ dig publicdnsname.mikrotik.ftscloud.ch

; <<>> DiG 9.10.6 <<>> publicdnsname.mikrotik.ftscloud.ch
...
;; ANSWER SECTION:
publicdnsname.mikrotik.ftscloud.ch. 300 IN A    212.51.159.27

$ dig customiprecord.mikrotik.ftscloud.ch
...
;; ANSWER SECTION:
customiprecord.mikrotik.ftscloud.ch. 300 IN A   192.168.1.1
```

## Requirements
- python3, pip

## Setup environment
First setup a local virtual env
```bash
virtualenv -p python3 .venv
```

Load the virtual env
```bash
source .venv/bin/activate
```

Now install the python requirements
```bash
pip install -r requirements.txt
```

Create the vault password file
```bash
# see keepass "api.mikrotik.ftscloud.ch vaultpass"
echo VAULTPASSWORD > .vault
```

## Usage
First - load the virtual environment 
```bash
source .venv/bin/activate
```
Now make sure you have your AWS credentials setup.
I am using named profiles in my AWS configuration - so I export the profile name when executing the ansible playbook.

Executing the playbook will setup all necessary cloudformation stacks and deploy an empty lambda
```bash
AWS_PROFILE=ftsystems ansible-playbook playbook.yml
```

### Tags
If you want to update a specific stack you can use ansible tags 
- acm : Update the certifficate stack
- gateway: Update the api gateway stack
- lambda: Update lambda stacks
- restapi: Update the restapi stacks

